
# target, lowes
# 无需跳转网页，无需使用Postman

from selenium.webdriver.chrome.options import Options  # 从options模块中调用Options类
import time
from selenium import webdriver
from bs4 import BeautifulSoup
import openpyxl
import requests

# 定义读取参数函数
def Input_Parameter(str):
    list = str.split(',')
    # if len(list) == 2:
    #     sublist = list
    sublist = list[1].split(':')
    List = [list[0], sublist[0], sublist[1]]
    return List

# 定义搜索评论参数函数
def Search_Parameter(str, i, list_results):
    list = str.split(',')
    if len(list) == 1:
        try:
            result = list_results[i][str]
            return result
        except KeyError:
            result = ""
            return result
        except IndexError:
            result = "None"
            return result

    else:
        try:
            result = list_results[i][list[0]][list[1]]
            return result
        except KeyError:
            result = ""
            return result
        except IndexError:
            result = "None"
            return result



# 定义寻找网站名函数
def Find_WebName(worksheet, webname):
    i = 0
    for row in worksheet.rows:
        i += 1
        if row[0].value == webname:
            break
    dic = {'product': 'B' + str(i), 'com_star': 'C' + str(i), 'P_id': 'D' + str(i), 'P_size': 'E' + str(i),
           'id_url': 'F' + str(i), 'page_url': 'G' + str(i), 'P_page': 'H' + str(i), 'end_url': 'I' + str(i),
           'R_review_tag': 'J' + str(i), 'R_reviewer': 'K' + str(i), 'R_star_rating': 'L' + str(i), 'R_title': 'M' + str(i),
           'R_content': 'N' + str(i), 'R_date': 'O' + str(i), 'R_helps': 'P' + str(i)}
    return i, dic

def get_id(value, url):
    value_list = value.split(',')
    url_list = url.split(value_list[0])
    i = int(value_list[1])
    j = int(value_list[2])
    item_id = url_list[-1][i:i+j]
    return item_id

# 读取参数文件
workbook = openpyxl.load_workbook('../parameters_0.xlsx')
worksheet = workbook.worksheets[0]

webname = input('请输入搜索网站名称：(target,lowes)')
i, dic = Find_WebName(worksheet, webname)  # 得到相应的参数所在行
# print('i:', i)
# print('dic:', dic)
item = input('请输入产品名称：')
url = input('请输入产品购买页链接：')
Pages = 3

# Cookie = "PHPStat_First_Time_10000011=1480428327337; PHPStat_Cookie_Global_User_Id=_ck16112922052713449617789740328; " \
#          "PHPStat_Return_Time_10000011=1480428327337; " \
#          "PHPStat_Main_Website_10000011=_ck16112922052713449617789740328%7C10000011%7C%7C%7C; " \
#          "VISITED_COMPANY_CODE=%5B%22600064%22%5D; VISITED_STOCK_CODE=%5B%22600064%22%5D; " \
#          "seecookie=%5B600064%5D%3A%u5357%u4EAC%u9AD8%u79D1; _trs_uv=ke6m_532_iw3ksw7h; " \
#          "VISITED_MENU=%5B%228451%22%2C%229055%22%2C%229062%22%2C%229729%22%2C%228528%22%5D "
#
# headers = {
#     'User-agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 '
#                   'Safari/537.36',
#     'Cookie': Cookie,
#     'Connection': 'keep-alive',
#     'Accept': '*/*',
#     'Accept-Encoding': 'gzip, deflate, sdch',
#     'Accept-Language': 'zh-CN,zh;q=0.8',
#     'Host': 'query.sse.com.cn',
#     'Referer': 'http://www.sse.com.cn/assortment/stock/list/share/'
# }

# 创建工作薄
wb = openpyxl.Workbook()
# 获取工作薄的活动表
sheet = wb.active
# 工作表重命名
sheet.title = item

# 解析购买链接
chrome_options = Options()  # 实例化Option对象
chrome_options.add_argument('--headless')  # 对浏览器的设置
# driver = webdriver.Chrome(options=chrome_options)
driver = webdriver.Chrome()
driver.get(url)
time.sleep(2)  # 等待两秒，等浏览器加缓冲载数据
pageSource = driver.page_source  # 获取完整渲染的网页源代码
bs = BeautifulSoup(pageSource, 'html.parser')  # 使用BeautifulSoup解析数据

product_value = Input_Parameter(worksheet[dic['product']].value)
product = bs.find(product_value[0], {product_value[1]: product_value[2]})
sheet['A1'] = product.text
# print('product:', sheet['A1'].value)
sheet['A2'] = url
star_value = Input_Parameter(worksheet[dic['com_star']].value)
star = bs.find(star_value[0], {star_value[1]: star_value[2]})
# print('star:',star)
if webname == 'lowes':
    sheet['A3'] = star.get('aria-label')
else:
    sheet['A3'] = star.text

# print('star:', sheet['A3'].value)

driver.close()  # 关闭浏览器

sheet['A4'] = 'reviewer'  # 加表头，给A3单元格赋值
sheet['B4'] = 'star_rating'
sheet['C4'] = 'review_title'
sheet['D4'] = 'review_content'
sheet['E4'] = 'review_date'
# sheet['F4'] = 'purchase_size'https://www.lowes.com/pd/allen-roth-Vallymede-2-Light-Nickel-Transitional-Vanity-Light/1001232830
sheet['F4'] = 'help'
item_id = get_id(worksheet[dic['P_id']].value, url)
# print('id:',item_id)

headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) '
                         'Chrome/61.0.3163.100 Safari/537.36'}
for i in range(Pages):

    if worksheet[dic['end_url']].value:
        url = worksheet[dic['id_url']].value + item_id + worksheet[dic['page_url']].value + str(
            int(worksheet[dic['P_page']].value) * i) + worksheet[dic['end_url']].value
    else:
        url = worksheet[dic['id_url']].value + item_id + worksheet[dic['page_url']].value + str(int(worksheet[dic['P_page']].value)*i)
    print('url:', url)
    if webname == 'target':
        results = requests.get(url)
    else:
        results = requests.get(url, headers=headers)
    # results = requests.get(url)
    print('results.status_code:', results.status_code)
    json_results = results.json()
    list_results = json_results[worksheet[dic['R_review_tag']].value]
    # print(list_results[0]['author']['nickname'])
    # print(list_results[0]['Rating'])
    if list_results == []:  # 当评论抓取完毕，跳出循环
        break

    for j in range(worksheet[dic['P_size']].value):
        # print('j=', j)
        reviewer = Search_Parameter(worksheet[dic['R_reviewer']].value, j, list_results)
        # print('reviewer:', reviewer)
        rating = Search_Parameter(worksheet[dic['R_star_rating']].value, j, list_results)
        # print('rating:',rating)
        title = Search_Parameter(worksheet[dic['R_title']].value, j, list_results)
        # print('title:', title)
        content = Search_Parameter(worksheet[dic['R_content']].value, j, list_results)
        # print('content', content)
        date = Search_Parameter(worksheet[dic['R_date']].value, j, list_results)
        # print('date:',date)
        helps = Search_Parameter(worksheet[dic['R_helps']].value, j, list_results)
        # print('helps:',helps)

        sheet.append([reviewer, rating, title, content, date, helps])
        if reviewer == 'None':
            break
        else:
            continue


wb.save('./results/' + webname + '_review_' + item + '.xlsx')


