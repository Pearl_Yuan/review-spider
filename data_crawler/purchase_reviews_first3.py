from selenium.webdriver.chrome.options import Options  # 从options模块中调用Options类
import time
from selenium import webdriver
from bs4 import BeautifulSoup
import openpyxl


# 定义读取参数函数
def Input_Parameter(str):
    list = str.split(',')
    # if len(list) == 2:
    #     sublist = list
    sublist = list[1].split(':')
    List = [list[0], sublist[0], sublist[1]]
    return List

# 定义寻找网站名函数
def Find_WebName(worksheet, webname):
    i = 0
    for row in worksheet.rows:
        i += 1
        if row[0].value == webname:
            break
    dic = {'product': 'B' + str(i), 'com_star': 'C' + str(i), 'com_review': 'D' + str(i), 'home': 'E' + str(i),
           'page': 'F' + str(i),  'review_tag': 'G' + str(i), 'reviewer': 'H' + str(i), 'star': 'I' + str(i),
           'title': 'J' + str(i), 'content': 'K' + str(i), 'date': 'L' + str(i), 'helps': 'M' + str(i)}
    return i, dic


# 读取参数文件
workbook = openpyxl.load_workbook('../parameters_0.xlsx')
worksheet = workbook.worksheets[0]

webname = input('请输入搜索网站名称：')
i, dic = Find_WebName(worksheet, webname)  # 得到相应的参数所在行
# print('i:', i)
# print('dic:', dic)
item = input('请输入产品名称：')
url = input('请输入产品购买页链接：')
Pages = 2

# 浏览器设置
chrome_options = Options()  # 实例化Option对象
chrome_options.add_argument('--headless')  # 对浏览器的设置
driver = webdriver.Chrome()  # 打开浏览器

driver.get(url)
time.sleep(2)  # 等待两秒，等浏览器加缓冲载数据

pageSource = driver.page_source  # 获取完整渲染的网页源代码
bs = BeautifulSoup(pageSource, 'html.parser')  # 使用BeautifulSoup解析数据
# all_reviews = bs.find('a', {'data-hook': 'see-all-reviews-link-foot'})
value_list = Input_Parameter(worksheet[dic['com_review']].value)
all_reviews = bs.find(value_list[0], {value_list[1]: value_list[2]})
# print(all_reviews)
all_reviews = all_reviews.get('href')  # 获得评论页网址
# print('all_reviews:', all_reviews)
# print('old:', worksheet['E4'].value + all_reviews + worksheet['F4'].value + str(1))
# print('new:', all_reviews + worksheet['F4'].value + str(1))

# 创建工作薄
wb = openpyxl.Workbook()
# 获取工作薄的活动表
sheet = wb.active
# 工作表重命名
sheet.title = item

product_value = Input_Parameter(worksheet[dic['product']].value)
product = bs.find(product_value[0], {product_value[1]: product_value[2]})

sheet['A1'] = product.text
sheet['A2'] = url
star_value = Input_Parameter(worksheet[dic['com_star']].value)
star = bs.find(star_value[0], {star_value[1]: star_value[2]})
sheet['A3'] = star.text

sheet['A4'] = 'reviewer'  # 加表头，给A3单元格赋值
sheet['B4'] = 'star_rating'
sheet['C4'] = 'review_title'
sheet['D4'] = 'review_content'
sheet['E4'] = 'review_date'
# sheet['F4'] = 'purchase_size'
sheet['F4'] = 'help'
# reviews = []

# # 浏览器设置
# chrome_options = Options()  # 实例化Option对象
# chrome_options.add_argument('--headless')  # 对浏览器的设置
# driver = webdriver.Chrome()  # 打开浏览器te

for x in range(Pages):

    if worksheet[dic['home']].value:
        driver.get(worksheet[dic['home']].value + all_reviews + worksheet[dic['page']].value + str(x + 1))
    else:
        driver.get(all_reviews + worksheet[dic['page']].value + str(x + 1))
    time.sleep(2)  # 等待两秒，等浏览器加缓冲载数据

    pageSource = driver.page_source  # 获取完整渲染的网页源代码
    bs = BeautifulSoup(pageSource, 'html.parser')  # 使用BeautifulSoup解析数据

    results_value = Input_Parameter(worksheet[dic['review_tag']].value)
    results = bs.find_all(results_value[0], {results_value[1]: results_value[2]})
    # print(results)
    if results == []:  # 当评论抓取完毕，跳出循环
        break

    for result in results:
        name_value = Input_Parameter(worksheet[dic['reviewer']].value)
        name = result.find(name_value[0], {name_value[1]: name_value[2]})
        # print('name.text:', name.text)
        Star_value = Input_Parameter(worksheet[dic['star']].value)
        Star = result.find(Star_value[0], {Star_value[1]: Star_value[2]})
        # print('star:', Star)
        title_value = Input_Parameter(worksheet[dic['title']].value)
        title = result.find(title_value[0], {title_value[1]: title_value[2]})
        # print('title.text:', title.text)
        comment_value = Input_Parameter(worksheet[dic['content']].value)
        comment = result.find(comment_value[0], {comment_value[1]: comment_value[2]})
        # print('comment.text:', comment.text)
        date_value = Input_Parameter(worksheet[dic['date']].value)
        date = result.find(date_value[0], {date_value[1]: date_value[2]})
        # print('date.text:', date.text)
        helps_value = Input_Parameter(worksheet[dic['helps']].value)
        helps = result.find(helps_value[0], {helps_value[1]: helps_value[2]})
        # print('helps.text:', helps.text)

        if name or Star or title or comment or date.text or helps.text:
            if name:
                name = name.text
            if 'stars' in Star.text or '.' in Star.text:
                Star = Star.text
            else:                          # 从标签中提取
                Star = Star.get('title')
            if title:
                title = title.text
            if comment:
                comment = comment.text
            if date:
                date = date.text
            if helps:
                helps = helps.text

        sheet.append([name, Star, title, comment, date, helps])

    # button = driver.find_element_by_class_name('a-last')  # 设置【Next page】按钮
    # button.click()  # 点击按钮

    if (x + 1) % 10 == 0:
        time.sleep(5)  # 每抓取10页数据，等待5秒，减轻服务器负担

wb.save('./results/' + webname + '_review_' + item + '.xlsx')

driver.close()  # 关闭浏览器



