1. parameters_1.xlsx：网站的相关参数
 （ 其中的网站使用的是 network->XHR中的数据 ）
   purchase_reviews_click_page.py, purchase_reviews_dynamic_page.py 均使用 parameters_1.xlsx 文件。


2. purchase_reviews_dynamic_page.py: 适用于target,lowes网站（动态网页）
  python purchase_reviews_dynamic_page.py → 输入：网站名；商品名；购买链接
                                                                          输出：评论汇总.xlsx

3. purchase_reviews_click_page.py：原本想在eBay和BESTBUY网站上运行，但前者难获得标签，后者运行失败，暂无解决办法。
 
############################################################################################

4. parameters_old.xlsx: 网站的相关参数
  （ 其中的网站使用的是 Elements 中的数据 ）
   purchase_reviews_first3.py, purchase_reviews_web.py 均使用 parameters_old.xlsx 文件。


5. purchase_reviews_first3.py：适用于 Amazon、ebay、Walmart网站
   python purchase_reviews_first3.py → 输入：网站名；商品名；购买链接
                                                            输出：评论汇总.xlsx

6. purchase_reviews_web.py: 适用于所有购买页与评论页不共享的网站 
                                          （目前：Amazon、ebay、Walmart、BESTBUY）
   python purchase_reviews_web.py → 输入：网站名；商品名；购买链接
                                                           输出：评论汇总.xlsx
