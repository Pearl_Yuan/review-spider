import requests
from selenium.webdriver.chrome.options import Options
import time
from selenium import webdriver
from bs4 import BeautifulSoup
import openpyxl


class WesiteDataProvider:
    def getFile(self, file):
        workbook = openpyxl.load_workbook(file)
        worksheet = workbook.worksheets[0]
        return worksheet

    def getWebisteData(self, url):

        # 配置浏览器
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        driver = webdriver.Chrome()

        # 加载浏览器
        driver.get(url)
        time.sleep(2)

        # 解析网页数据
        pageSource = driver.page_source
        bs = BeautifulSoup(pageSource, 'html.parser')

        return url, bs, driver


# 在表格文件中寻找 Amazon, ebay, Walmart, BESTBUY 的网站参数
def findWebNameA(worksheet, webname):
    i = 0
    for row in worksheet.rows:
        i += 1
        if row[0].value == webname:  # 找到网站所在的行
            break
    dic = {'product': 'B' + str(i), 'P_star': 'C' + str(i), 'P_review': 'D' + str(i), 'R_home': 'E' + str(i),
           'R_page': 'F' + str(i), 'review_tag': 'G' + str(i), 'reviewer': 'H' + str(i), 'star': 'I' + str(i),
           'title': 'J' + str(i), 'content': 'K' + str(i), 'date': 'L' + str(i), 'helps': 'M' + str(i)}
    return dic


# 在表格文件中寻找 target, lowes 的网站参数
def findWebNameB(worksheet, webname):
    i = 0
    for row in worksheet.rows:
        i += 1
        if row[0].value == webname:
            break
    dic = {'product': 'B' + str(i), 'P_star': 'C' + str(i), 'review_tag': 'G' + str(i), 'reviewer': 'H' + str(i),
           'star': 'I' + str(i), 'title': 'J' + str(i), 'content': 'K' + str(i), 'date': 'L' + str(i), 'helps': 'M'
            + str(i), 'P_id': 'N' + str(i), 'P_size': 'O' + str(i), 'id_url': 'P' + str(i), 'page_url': 'Q' + str(i),
           'P_page': 'R' + str(i), 'end_url': 'S' + str(i)}
    return dic


# 参数解析函数一
def Input_Parameter(str):
    list = str.split(',')
    sublist = list[1].split(':')
    List = [list[0], sublist[0], sublist[1]]
    return List


# 参数解析函数二
def Search_Parameter(str, i, list_results):
    list = str.split(',')
    if len(list) == 1:
        try:
            result = list_results[i][str]
            return result
        except KeyError:
            result = ""
            return result
        except IndexError:
            result = "None"
            return result

    else:
        try:
            result = list_results[i][list[0]][list[1]]
            return result
        except KeyError:
            result = ""
            return result
        except IndexError:
            result = "None"
            return result


# 从购买链接中获取产品id
def get_id(value, url):
    value_list = value.split(',')
    url_list = url.split(value_list[0])
    i = int(value_list[1])
    j = int(value_list[2])
    item_id = url_list[-1][i:i + j]
    return item_id
