from selenium.webdriver.chrome.options import Options
import time
from selenium import webdriver
from bs4 import BeautifulSoup
import openpyxl
import os
from data_providers.WesiteDataProvider import *


class targetCrawler:
    def __init__(self, page=3, webname='target', worksheet=None, itemName='test', saveFolder='../results/'):

        self.page = page
        self.webname = webname
        self.worksheet = worksheet
        self.itemName = itemName
        self.saveFolder = saveFolder
        if not os.path.exists(self.saveFolder):
            os.mkdir(self.saveFolder)

        # 获取爬虫所需的参数
        self.dic = findWebNameB(self.worksheet, self.webname)

        # 创建表格用作评论内容的储存
        self.wb = openpyxl.Workbook()
        self.sheet = self.wb.active
        self.sheet.title = self.itemName

        # 初始化表格
        self.sheet['A4'] = 'reviewer'
        self.sheet['B4'] = 'star_rating'
        self.sheet['C4'] = 'review_title'
        self.sheet['D4'] = 'review_content'
        self.sheet['E4'] = 'review_date'
        self.sheet['F4'] = 'help'

    def crawler(self, url, bs, driver):

        # 商品名称（较长）
        product_value = Input_Parameter(self.worksheet[self.dic['product']].value)
        product = bs.find(product_value[0], {product_value[1]: product_value[2]})
        # itemName：从名称中提取出第一个单词(一般为品牌名)
        productName = product.text.strip()
        list = productName.split(' ')
        self.itemName = list[0]

        # 商品id
        itemID = get_id(self.worksheet[self.dic['P_id']].value, url)

        # 综合星级评定
        star_value = Input_Parameter(self.worksheet[self.dic['P_star']].value)
        star = bs.find(star_value[0], {star_value[1]: star_value[2]})

        # driver.close()

        # 填充表头
        self.sheet['A1'] = productName
        self.sheet['A2'] = url
        self.sheet['A3'] = star.text

        # 按页抓取评论内容
        for x in range(self.page):

            url_new = self.worksheet[self.dic['id_url']].value + itemID + self.worksheet[self.dic['page_url']].value + \
                      str(int(self.worksheet[self.dic['P_page']].value) * x) + self.worksheet[self.dic['end_url']].value

            # 解析json数据
            results = requests.get(url_new)
            json_results = results.json()
            list_results = json_results[self.worksheet[self.dic['review_tag']].value]

            # 当评论抓取完毕，跳出循环
            if not list_results:
                break

            # 抓取当页评论内容
            for i in range(self.worksheet[self.dic['P_size']].value):

                reviewer = Search_Parameter(self.worksheet[self.dic['reviewer']].value, i, list_results)
                rating = Search_Parameter(self.worksheet[self.dic['star']].value, i, list_results)
                title = Search_Parameter(self.worksheet[self.dic['title']].value, i, list_results)
                content = Search_Parameter(self.worksheet[self.dic['content']].value, i, list_results)
                date = Search_Parameter(self.worksheet[self.dic['date']].value, i, list_results)
                helps = Search_Parameter(self.worksheet[self.dic['helps']].value, i, list_results)

                self.sheet.append([reviewer, rating, title, content, date, helps])

                if reviewer == 'None':
                    break
                else:
                    continue

        self.wb.save(self.saveFolder + self.webname + '_review_' + self.itemName + '.xlsx')
        driver.close()  # 关闭浏览器


if __name__ == '__main__':

    url = input('请输入产品购买页链接：')

    file = WesiteDataProvider()
    worksheet = file.getFile('../parameters.xlsx')  # 读取参数所在文件
    url, bs, driver = file.getWebisteData(url)
    result = targetCrawler(worksheet=worksheet)
    result.crawler(url, bs, driver)
