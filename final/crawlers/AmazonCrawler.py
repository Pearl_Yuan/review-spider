from selenium.webdriver.chrome.options import Options  # 从options模块中调用Options类
import time
from selenium import webdriver
from bs4 import BeautifulSoup
import openpyxl
import os
from data_providers.WesiteDataProvider import *


class AmazonCrawler:
    def __init__(self, page=3, webname='Amazon', worksheet=None, itemName='test', saveFolder='../results/'):

        self.page = page
        self.webname = webname
        self.worksheet = worksheet
        self.itemName = itemName
        self.saveFolder = saveFolder
        if not os.path.exists(self.saveFolder):
            os.mkdir(self.saveFolder)

        # 获取爬虫所需的参数
        self.dic = findWebNameA(self.worksheet, self.webname)

        # 创建表格用作评论内容的储存
        self.wb = openpyxl.Workbook()
        self.sheet = self.wb.active
        self.sheet.title = self.itemName

        # 初始化表格
        self.sheet['A4'] = 'reviewer'
        self.sheet['B4'] = 'star_rating'
        self.sheet['C4'] = 'review_title'
        self.sheet['D4'] = 'review_content'
        self.sheet['E4'] = 'review_date'
        self.sheet['F4'] = 'help'

    def crawler(self, url, bs, driver):

        # 物品名称（较长）
        product_value = Input_Parameter(self.worksheet[self.dic['product']].value)
        product = bs.find(product_value[0], {product_value[1]: product_value[2]})
        # 从名称中提取出第一个单词(一般为品牌名)为itemName
        productName = product.text.strip()
        list = productName.split(' ')
        self.itemName = list[0]

        # 评论页网址
        value_list = Input_Parameter(self.worksheet[self.dic['P_review']].value)
        all_reviews = bs.find(value_list[0], {value_list[1]: value_list[2]})
        all_reviews = all_reviews.get('href')

        # 综合星级评定
        star_value = Input_Parameter(self.worksheet[self.dic['P_star']].value)
        star = bs.find(star_value[0], {star_value[1]: star_value[2]})

        # 填充表头
        self.sheet['A1'] = productName
        self.sheet['A2'] = url
        self.sheet['A3'] = star.text

        # 按页抓取评论内容
        for x in range(self.page):

            # 加载浏览器
            driver.get(self.worksheet[self.dic['R_home']].value + all_reviews + self.worksheet[self.dic['R_page']].value + str(x + 1))
            time.sleep(2)

            # 解析网页数据
            pageSource = driver.page_source
            bs = BeautifulSoup(pageSource, 'html.parser')

            results_value = Input_Parameter(self.worksheet[self.dic['review_tag']].value)
            results = bs.find_all(results_value[0], {results_value[1]: results_value[2]})

            # 当评论抓取完毕，跳出循环
            if not results:
                break

            # 抓取当页评论内容
            for result in results:
                name_value = Input_Parameter(self.worksheet[self.dic['reviewer']].value)
                name = result.find(name_value[0], {name_value[1]: name_value[2]})

                Star_value = Input_Parameter(self.worksheet[self.dic['star']].value)
                Star = result.find(Star_value[0], {Star_value[1]: Star_value[2]})

                title_value = Input_Parameter(self.worksheet[self.dic['title']].value)
                title = result.find(title_value[0], {title_value[1]: title_value[2]})

                comment_value = Input_Parameter(self.worksheet[self.dic['content']].value)
                comment = result.find(comment_value[0], {comment_value[1]: comment_value[2]})

                date_value = Input_Parameter(self.worksheet[self.dic['date']].value)
                date = result.find(date_value[0], {date_value[1]: date_value[2]})

                helps_value = Input_Parameter(self.worksheet[self.dic['helps']].value)
                helps = result.find(helps_value[0], {helps_value[1]: helps_value[2]})

                # 避免其中某项为空导致报错
                if name or Star or title or comment or date.text or helps.text:
                    if name:
                        name = name.text
                    if Star:
                        Star = Star.text
                    if title:
                        title = title.text
                    if comment:
                        comment = comment.text
                    if date:
                        date = date.text
                    if helps:
                        helps = helps.text

                self.sheet.append([name, Star, title, comment, date, helps])

            # 每抓取10页数据，等待5秒，减轻服务器负担
            if (x + 1) % 10 == 0:
                time.sleep(5)

        self.wb.save(self.saveFolder + self.webname + '_review_' + self.itemName + '.xlsx')
        driver.close()  # 关闭浏览器


if __name__ == '__main__':

    url = input('请输入产品购买页链接：')

    file = WesiteDataProvider()
    worksheet = file.getFile('../parameters.xlsx')  # 读取参数所在文件
    url, bs, driver = file.getWebisteData(url)
    result = AmazonCrawler(worksheet=worksheet)
    result.crawler(url, bs, driver)
