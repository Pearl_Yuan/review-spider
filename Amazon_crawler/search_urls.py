from selenium.webdriver.chrome.options import Options  # 从options模块中调用Options类
import time
from selenium import webdriver
from bs4 import BeautifulSoup
import openpyxl

#  MAIN-CARDS  MAIN-SEARCH_RESULTS  MAIN-SHOPPING_ADVISER  MAIN-VIDEO_SINGLE_PRODUCT
home = 'https://www.amazon.com/'
item = input('请输入想要搜索的产品名称：')
previous_button = None

# 创建工作薄
wb = openpyxl.Workbook()
# 获取工作薄的活动表
sheet = wb.active
# 工作表重命名
sheet.title = item
sheet['A1'] = 'pages'  # 加表头，给A1单元格赋值
sheet['B1'] = 'search urls'  # 加表头，给B1单元格赋值

# 浏览器设置
chrome_options = Options()  # 实例化Option对象
chrome_options.add_argument('--headless')  # 对浏览器的设置
driver = webdriver.Chrome()  # 打开浏览器

driver.get(home)
time.sleep(2)  # 等待两秒，等浏览器加缓冲载数据

pageSource = driver.page_source  # 获取完整渲染的网页源代码
bs = BeautifulSoup(pageSource, 'html.parser')  # 使用BeautifulSoup解析数据
assistant = driver.find_element_by_id('twotabsearchtextbox')  # 找到输入框位置
assistant.send_keys(item)  # 输入文字
buttons = driver.find_elements_by_class_name('nav-input')  # 设置【搜索】按钮
for b in buttons:
    # print(b)
    if b != assistant:
        button = b
        button.click()  # 点击按钮

for i in range(22):
    time.sleep(2)  # 等待两秒，等浏览器加缓冲载数据
    n = driver.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
    # print('当前句柄: ', n)  # 会打印所有的句柄
    driver.switch_to_window(n[-1])
    # Now_window = driver.current_window_handle
    # print('Now_current:', Now_window)

    pageSource = driver.page_source  # 获取完整渲染的网页源代码
    bs2 = BeautifulSoup(pageSource, 'html.parser')  # 使用BeautifulSoup解析数据
    Next_button = driver.find_element_by_class_name('a-last')
    # print('Next_button', Next_button)
    if previous_button == Next_button:
        print('抓取结束!')
        break
    # print(bs2)
    products_1 = bs2.find('span', {'cel_widget_id': 'MAIN-SAFE_FRAME'})
    check = True
    if products_1:
        # 第一行链接抓取
        results = products_1.find_all('div', {'style': 'border-left: 24px solid transparent;'})
        if results != []:  # 确认第一行链接已抓取
            check = None
        for result in results:
            tops = result.find('a', {'target': '_top'})
            url = tops.get('href')
            # print('第1-%d条链接：%s' % (i, url))
            sheet.append([i+1, url])
    if check:  # 当上一种方式没搜索到时，切换另一种搜索方式
        products_1 = bs2.find('span', {'cel_widget_id': 'MAIN-CARDS'})
        if products_1 != None:
            results = products_1.find_all(class_='_multi-card-creative-desktop_DesktopGridColumn_gridColumn__2evuV')
            for result in results:
                tops = result.find('a')
                url = tops.get('href')
                # print('第1-%d条链接：%s' % (i, url))
                sheet.append([i + 1, url])

    products_2 = bs2.find_all('span', {'cel_widget_id': 'MAIN-SEARCH_RESULTS'})
    products_3 = bs2.find('span', {'cel_widget_id': 'MAIN-FEATURED_ASINS_LIST'})
    products_4 = bs2.find('span', {'cel_widget_id': 'MAIN-SHOPPING_ADVISER'})


    if products_2:
        # 第二类、普通链接抓取
        for product in products_2:
            # sb_1REHiRA7 sb_1G_Jnizf
            result = product.find(class_='a-link-normal a-text-normal')
            url = home + result.get('href')
            # print('第2-%d条链接：%s' % (i, url))
            sheet.append([i+1, url])

    if products_3:
        # 第三类、行链接抓取
        middle = products_3.find_all('li', {'role': 'listitem'})
        for mid in middle:
            tops = mid.find(class_='a-link-normal a-text-normal')
            url = home + tops.get('href')
            # print('第3-%d条链接：%s' % (i, url))
            sheet.append([i+1, url])

    if products_4:
        # 第四类、行链接抓取
        middle = products_4.find_all('li', {'role': 'listitem'})
        for mid in middle:
            tops = mid.find(class_='a-link-normal a-text-normal')
            url = home + tops.get('href')
            # print('第3-%d条链接：%s' % (i, url))
            sheet.append([i+1, url])

    previous_button = driver.find_element_by_class_name('a-last')  # 设置【Next page】按钮
    # print('previous_button',previous_button)
    Next_button.click()  # 点击按钮


wb.save('Amazon_search_' + item + '.xlsx')
# driver.close()  # 关闭浏览器

