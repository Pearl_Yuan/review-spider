from selenium.webdriver.chrome.options import Options  # 从options模块中调用Options类
import time
from selenium import webdriver
from bs4 import BeautifulSoup
import openpyxl

item = input('请输入产品名称：')
url = input('请输入产品购买页链接：')
Pages = 1000

# 浏览器设置
chrome_options = Options()  # 实例化Option对象
chrome_options.add_argument('--headless')  # 对浏览器的设置
driver = webdriver.Chrome()  # 打开浏览器

driver.get(url)
time.sleep(2)  # 等待两秒，等浏览器加缓冲载数据

pageSource = driver.page_source  # 获取完整渲染的网页源代码
bs = BeautifulSoup(pageSource, 'html.parser')  # 使用BeautifulSoup解析数据
all_reviews = bs.find('a', {'data-hook': 'see-all-reviews-link-foot'})
all_reviews = all_reviews.get('href')
# print(all_reviews)


# 创建工作薄
wb = openpyxl.Workbook()
# 获取工作薄的活动表
sheet = wb.active
# 工作表重命名
sheet.title = item

sheet['A4'] = 'reviewer'  # 加表头，给A3单元格赋值
sheet['B4'] = 'star_rating'
sheet['C4'] = 'review_title'
sheet['D4'] = 'review_content'
sheet['E4'] = 'review_date'
sheet['F4'] = 'purchase_size'
sheet['G4'] = 'help'
# reviews = []

# # 浏览器设置
# chrome_options = Options()  # 实例化Option对象
# chrome_options.add_argument('--headless')  # 对浏览器的设置
# driver = webdriver.Chrome()  # 打开浏览器

for x in range(Pages):

    driver.get('https://www.amazon.com' + all_reviews + '&pageNumber=' + str(x + 1))
    time.sleep(2)  # 等待两秒，等浏览器加缓冲载数据

    pageSource = driver.page_source  # 获取完整渲染的网页源代码
    bs = BeautifulSoup(pageSource, 'html.parser')  # 使用BeautifulSoup解析数据
    product = bs.find('a', {'data-hook': 'product-link'})
    sheet['A1'] = product.text
    sheet['A2'] = 'https://www.amazon.com' + product.get('href')
    star = bs.find('span', {'data-hook': 'rating-out-of-text'})
    sheet['A3'] = star.text
    results = bs.find_all("div", class_='a-section review aok-relative')  # 最小级父标签
    # print(results)
    if results == []:  # 当评论抓取完毕，跳出循环
        break

    for result in results:
        name = result.find(class_='a-profile-name')
        star = result.find(class_='a-icon-alt')
        title = result.find('a', {'data-hook': 'review-title'})
        comment = result.find('span', {'data-hook': 'review-body'})
        date = result.find('span', {'data-hook': 'review-date'})
        purchase_size = result.find('span', {'data-hook': 'format-strip'})
        helps = result.find('span', {'class': 'review-comment-total aok-hidden'})

        if purchase_size:
            purchase_size = purchase_size.text
        sheet.append([name.text, star.text, title.text, comment.text, date.text, purchase_size, helps.text])

    button = driver.find_element_by_class_name('a-last')  # 设置【Next page】按钮
    button.click()  # 点击按钮

    if (x + 1) % 10 == 0:
        time.sleep(5)  # 每抓取10页数据，等待5秒，减轻服务器负担

wb.save('Amazon_review_' + item + '.xlsx')

driver.close()  # 关闭浏览器



